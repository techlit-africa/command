module.exports = {
  extends: ['standard', 'plugin:react/recommended', 'plugin:react-pug/all'],
  plugins: ['react', 'react-pug'],
  rules: {
    'react/prop-types': 0,

    'react-pug/no-undef': 1,
    'react-pug/prop-types': 0,
    'react-pug/quotes': 0,
    'react-pug/empty-lines': 0,
    'react-pug/no-interpolation': 0,

    camelcase: 1,
    eqeqeq: 1,
    'eol-last': ['warn', 'always'],
    'no-multiple-empty-lines': ['warn', {max: 1}],
    'object-curly-spacing': ['warn', 'never'],
    'object-curly-newline': ['warn', {consistent: true}],
    'space-before-function-paren': ['warn', {
      anonymous: 'always',
      named: 'never',
      asyncArrow: 'always',
    }],
    'function-call-argument-newline': ['warn', 'consistent'],
    'no-unused-vars': ['warn', {varsIgnorePattern: '^_'}],
    'comma-dangle': ['warn', 'always-multiline'],
  },
  globals: {
    React: 'readonly',
    ErrorBoundary: 'readonly',
    Icon: 'readonly',

    NODE_ENV: 'readonly',
    PDD_URL: 'readonly',
    PDD_WS_URL: 'readonly',
    SOCIAL_URL: 'readonly',
  },
}

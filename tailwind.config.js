module.exports = {
  // mode: "jit",
  separator: '_',
  purge: ['./**/*.{js,ts,jsx,tsx}'],
  darkMode: 'class',
  variants: {
    extend: {
      display: ['dark'],
    },
  },
  plugins: [],
}

import {library, config} from '@fortawesome/fontawesome-svg-core'

import {
  faMoon,
  faSun
} from '@fortawesome/free-solid-svg-icons'

library.add(
  faMoon,
  faSun
)

config.autoAddCss = false

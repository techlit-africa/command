import useSWR, {mutate} from 'swr'
import {useSocket} from 'lib/socket'

export const usePeers = () => {
  const {data: peersAndMe} = useSWR('/users')
  const {data: profile} = useSWR('/users/me')
  const {socketHandlers} = useSocket()

  const [peersByHostname, setPeersByHostname] = React.useState({})

  React.useEffect(() => {
    if (!peersAndMe) return

    const newByHostname =
      Object.values(peersAndMe)
        .reduce((byHostname, peer) => {
          byHostname[peer.hostname] = peer
          return byHostname
        }, {})

    setPeersByHostname(newByHostname)
  }, [peersAndMe])

  const peerName = React.useCallback((hostname) => {
    return peersByHostname[hostname]?.name || 'Guest'
  }, [peersByHostname])

  const peers = React.useMemo(() => {
    if (!peersAndMe || !profile) return []
    return Object.values(peersAndMe)
      .filter((peer) => peer.hostname !== profile.hostname)
      .sort((peer) => peer.online ? 0 : 1)
  }, [peersAndMe, profile])

  React.useEffect(() => {
    socketHandlers.PeerJoined = (peer) =>
      mutate('/users', (px) => (px && {
        ...px,
        [peer.address]: peer,
      }))

    socketHandlers.PeerLeft = (peer) =>
      mutate('/users', (px) => (px && {
        ...px,
        [peer.address]: peer,
      }))
  }, [])

  return {peers, peersAndMe, peersByHostname, peerName}
}

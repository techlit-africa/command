import {provideContext} from 'lib/provideContext'

let socket
const socketHandlers = {}

const connect = () => {
  if (socket?.readyState === 1) return

  socket = new window.WebSocket(PDD_WS_URL)

  socket.onclose = () => connect()
  socket.onerror = (e) => console.error(e)

  socket.onmessage = ({data: payload}) => {
    const {type, data, meta} = JSON.parse(payload)
    socketHandlers[type]?.({type, data, meta})
  }
}

export const {Provider: SocketProvider, useContext: useSocket} = provideContext(() => {
  React.useEffect(() => { connect() }, [])
  return {socket, socketHandlers}
})

import {SWRConfig} from 'swr'
import {fetch} from 'lib/http'

export const SWRProvider = ({children}) => {
  const swrConfig = {
    fetcher: fetch,
    suspense: true,
    revalidateOnMount: true,
    dedupingInterval: 100,
    focusThrottleInterval: 100,
    refreshInterval: 8000,
  }

  return pug`
    SWRConfig(value=swrConfig) #{children}
  `
}

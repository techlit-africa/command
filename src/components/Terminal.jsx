export const Terminal = ({
  lines = [],
}) => {
  const focus = () => {}
  const sanitizedLines = React.useMemo(
    () => lines.map((line, i) => ({html: line, key: i})),
    [lines],
  )

  return pug`
    .h-full.w-full.flex.flex-col.overflow-auto(onClick=focus)
      for line in sanitizedLines
        pre.whitespace-pre-wrap(
          key=line.key
          dangerouslySetInnerHTML={__html: line.html}
        )
  `
}
import {toggleLightsOn} from 'lib/theme'
import ReactConsole from '@webscopeio/react-console'
import {Terminal} from './Terminal'
import {vol} from 'fs'
import path from 'path'

const welcomeMessage = `
🎮 Welcome to UNKNOWN PROJECT! 🎮
   ------------ | ------------

  Follow the instructions 👉
  to learn hacker skills

`

const instructions = `Run these 👇 commands for xp ✨

`
const initialGoals = [
  'whoami',
  'pwd',
  'ls',
  'cd me',
  'pwd',
  'ls',
  'cat gold',
  'cat xp',
  'cat xp-target',
  'cat name',
]

export const App = () => {
  const [{fsChangedAt, pwd, goals, hostname}, update] = React.useReducer(
    (s1, s2) => ({...s1, ...s2}),
    {
      hostname: 'tutorial',
      goals: [...initialGoals],
      pwd: '/',
      fsChangedAt: new Date(),
    },
  )

  const fsReady = React.useRef()
  if (!fsReady.current) {
    fsReady.current = true
    vol.fromJSON({
      '/me/name': 'Tyler',
      '/me/mana': '1',
      '/me/mana-total': '1',
      '/me/str': '1',
      '/me/str-total': '1',
      '/me/hp': '1',
      '/me/hp-total': '1',
      '/me/gold': '0',
      '/me/xp': '0',
      '/me/xp-target': initialGoals.length.toString(),
      '/me/lvl': '0',
    })
  }
  const touchFs = () => update({fsChangedAt: new Date()})

  const absPath = (pwd, p) =>
    path.normalize(
      path.isAbsolute(p) ? p : path.join(pwd, p),
    )

  const cat = (p) =>
    vol.readFileSync(absPath(pwd, p)).toString()

  const write = (p, val) => {
    touchFs()
    vol.writeFileSync(absPath(pwd, p), val.toString())
  }

  const inc = (p) => write(p, parseInt(cat(p)) + 1)
  const dec = (p) => write(p, parseInt(cat(p)) - 1)

  const cd = (p = '') => {
    const abs = absPath(pwd, p)
    if (vol.lstatSync(abs)?.isDirectory()) {
      update({pwd: abs})
    } else {
      return `Not a directory: ${p}`
    }
  }

  const ls = (p = '') => {
    const abs = absPath(pwd, p)
    if (vol.lstatSync(abs)?.isDirectory()) {
      return vol.readdirSync(absPath(pwd, p))?.join(' ')
    } else {
      return abs
    }
  }

  const cmd = (cmd, f) => (...args) => {
    try {
      const out = f(cmd, ...args)

      const goal = [cmd, ...args].join(' ').trim()
      const index = goals.indexOf(goal)
      if (index > -1) {
        inc('/me/xp')
        goals.splice(index, 1)
        update({goals})
      }
      console.log(parseInt(cat('/me/xp')), initialGoals.length)
      if (parseInt(cat('/me/xp')) >= initialGoals.length) {
        write('/me/xp', 0)
        inc('/me/lvl')
      }

      return out
    } catch (e) {
      return e.toString()
    }
  }

  const statCmd = (stat) => ({
    description: `modify your ${stat}`,
    fn: cmd(stat, (cmd, sub) => {
      const oldStat = parseInt(cat(`/me/${stat}`))
      const statTotal =
        stat === 'gold'
          ? 1000
          : stat === 'xp'
            ? parseInt(cat(`/me/${stat}-target`))
            : parseInt(cat(`/me/${stat}-total`))

      if (sub === 'up' && oldStat < statTotal) inc(`/me/${stat}`)
      if (sub === 'down' && oldStat > 1) dec(`/me/${stat}`)
      return cat(`/me/${stat}`)
    }),
  })

  const commands = React.useMemo(() => ({
    whoami: {fn: cmd('whoami', () => 'Tyler'), description: ''},
    pwd: {fn: cmd('pwd', () => { touchFs(); return pwd }), description: ''},
    cd: {fn: cmd('cd', (_, ...args) => cd(...args)), description: ''},
    ls: {fn: cmd('ls', (_, ...args) => ls(...args)), description: ''},
    cat: {fn: cmd('cat', (_, ...args) => cat(...args)), description: ''},
    mana: statCmd('mana'),
    str: statCmd('str'),
    hp: statCmd('hp'),
    gold: statCmd('gold'),
    xp: statCmd('xp'),
  }), [pwd, fsChangedAt])

  const textStyle = {
    lineHeight: '1.25em',
    fontSize: '1.25em',
    whiteSpace: 'pre-wrap',
  }

  return pug`
    .w-full.h-screen.flex.flex-row.font-mono.text-gray-900.dark_text-gray-100.bg-gray-200.dark_bg-gray-900
      .h-screen(className='w-1/2')
        .w-full.h-full
          ReactConsole(
            autoFocus
            welcomeMessage=welcomeMessage
            commands=commands
            prompt=hostname+' $'
            wrapperStyle={height: '100%', width: '100%'}
            lineStyle=textStyle
            inputStyle=textStyle
            promptStyle=textStyle
          )

      .h-screen.flex.flex-col.border-l-2.border-gray-500(className='w-1/2')
        .w-full.h-60.p-3.bg-gray-100.dark_bg-black.border-b-2.border-gray-500
          Terminal(lines=[instructions, ...goals.map(x => '  '+x)])

        .w-full.h-12.flex.flex-row.items-center.justify-between.px-2.bg-gray-100.dark_bg-gray-900.border-b-2.border-gray-500
          .flex.flex-row.items-center
            .text-2xl.pr-2 ✨
            .text-xl.pr-2 #{cat('/me/xp')}/#{cat('/me/xp-target')}
            .text-xs.text-gray-500 (xp)
          .flex.flex-row.items-center
            .text-2xl.pr-2 🎖️
            .text-xl.pr-2 #{cat('/me/lvl')}
            .text-xs.text-gray-500 (lvl)
          .flex.flex-row.items-center
            .text-2xl.pr-2 💰
            .text-xl.pr-2 #{cat('/me/gold')}
            .text-xs.text-gray-500 (gold)

        .w-full.flex-1.bg-gray-200.dark_bg-gray-800.border-b-2.border-gray-500
          .w-full.h-full.flex.items-center.justify-center.text-8xl 🧙

        .w-full.h-12.flex.flex-row.items-center.justify-between.px-2.bg-gray-100.dark_bg-gray-900
          .flex.flex-row.items-center
            .text-2xl.pr-2 🔮
            .text-xl.pr-2 #{cat('/me/mana')}/#{cat('/me/mana-total')}
            .text-xs.text-gray-500 (mana)
          .flex.flex-row.items-center
            .text-2xl.pr-2 💪
            .text-xl.pr-2 #{cat('/me/str')}/#{cat('/me/str-total')}
            .text-xs.text-gray-500 (str)
          .flex.flex-row.items-center
            .text-2xl.pr-2 💚
            .text-xl.pr-2 #{cat('/me/hp')}/#{cat('/me/hp-total')}
            .text-xs.text-gray-500 (hp)
  `
}

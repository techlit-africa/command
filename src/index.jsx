import {render} from 'react-dom'
import {SWRProvider} from 'lib/swr'
import {SocketProvider} from 'lib/socket'
import {App} from 'components/App'
import {syncLightsOn} from 'lib/theme'

import 'styles/index.css'
import 'init/fontawesome'
import 'init/dayjs'

syncLightsOn()

render(
  pug`
    React.StrictMode
      SWRProvider
        SocketProvider
          App
  `,
  document.getElementById('root'),
)

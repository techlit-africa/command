# Getting Started

0. have node >=16 installed via nvm and yarn installed globally via nvm
1. run `yarn install && yarn dev`

# Goals

1. Terminal/Console vs Shell/Repl/Program -- clear vs ctrl-l, exit vs ctrl-d
1. Command vs options & arguments vs control flow
1. Std{in,out,err}, echo, cat, touch, pipe, redirect
1. Filesystems -- files vs directories, root, absolute vs relative, home alias
1. Text editors, init scripts, aliases
1. PATH, extensions vs shebangs, scripting
1. Permissions -- users, groups, shorthand
1. grep, sed, find, locate, awk, etc.
1. Conditions, Loops
1. Expansions, globs, string ops
1. Same things in Python, Node, Ruby
1. ping, curl, ssh, scp
